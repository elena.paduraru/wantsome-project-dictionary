package utils;

import java.util.*;

public class Utils {

    public static Set<String> removeDuplicates(List<String> allLines) {
        Set<String> wordsSet = new HashSet<>();

        for (String line : allLines) {
            wordsSet.add(line);
        }

        return wordsSet;
    }

    public static Set<String> findWordsThatContainsASpecificString(Set<String> wordsSet, String word) {
        Set<String> result = new HashSet<>();
        for (String line : wordsSet) {
            if (line.contains(word)) {
                result.add(line);
            }
        }

        return result;
    }

    /**
     * ex: ana, unu
     *
     * @param wordsSet
     * @return
     */
    public static Set<String> findAllPalindromes(Set<String> wordsSet) {
        Set<String> result = new HashSet<>();

        for (String line : wordsSet) {
            StringBuilder reverseWord = new StringBuilder(line).reverse();
            if (line.equals(reverseWord.toString())) {
                result.add(line);
            }
        }

        return result;
    }

    public static String sortLetters(String word) {
        char[] letterSorted = word.toCharArray();
        Arrays.sort(letterSorted);

        return String.valueOf(letterSorted);
    }

    public static Map<String, List<String>> getAnagrams(Set<String> wordsSet) {
        Map<String, List<String>> anagrams = new HashMap<>();

        for (String word : wordsSet) {
            String sorted = sortLetters(word);

            List<String> listOfWords;
            if (!anagrams.containsKey(sorted)) {
                listOfWords = new ArrayList<>();
            } else {
                listOfWords = anagrams.get(sorted);
            }
            listOfWords.add(word);
            anagrams.put(sorted, listOfWords);
        }

        return anagrams;
    }
}
