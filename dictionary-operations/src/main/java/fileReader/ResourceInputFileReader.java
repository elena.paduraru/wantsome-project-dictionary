package fileReader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ResourceInputFileReader {

    public List<String> readFile(String input) throws IOException {
        List<String> result = new ArrayList<String>();

        ClassLoader classLoader = ResourceInputFileReader.class.getClassLoader();
        URL resource = classLoader.getResource(input);
        File file;

        if(resource == null){
            throw new IllegalArgumentException("file not found!");
        } else{
            file = new File(resource.getFile());
        }

        try (FileReader reader = new FileReader(file);
             BufferedReader br = new BufferedReader(reader)){

            String line;
            while((line = br.readLine()) != null){
                result.add(line);
            }
        }

        return result;
    }
}
