import dictionaryOperations.AnagramDictionaryOperation;
import dictionaryOperations.DictionaryOperation;
import dictionaryOperations.PalindromeDictionaryOperation;
import dictionaryOperations.SearchDictionaryOperation;
import fileReader.ResourceInputFileReader;
import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws IOException {
        System.out.println("This is my main class");


        //citim de la tastatura
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
     //   String userInput = in.readLine();

        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("dex.txt");
        Set<String> wordsNoDuplicate = Utils.removeDuplicates(words);

        while(true){
            System.out.println("Menu: \n" +
                    "1. Search \n" +
                    "2. Find all palindromes\n" +
                    "3. Anagrame\n" +
                    "0. Exit");

            String userInput = in.readLine();
            if(userInput.equals("0")){
                System.out.println("La revedere!");
                break;
            }

            DictionaryOperation operation = null;

            switch (userInput){
                case "1":
                    operation = new SearchDictionaryOperation(wordsNoDuplicate, in);
                    break;
                case "2":
                    operation = new PalindromeDictionaryOperation(wordsNoDuplicate);
                    break;
                case "3":
                    operation = new AnagramDictionaryOperation(wordsNoDuplicate, in);
                    break;
                default:
                    System.out.println("Invalid option");
            }

            if(operation != null){
                operation.run();
            }

        }


/*        System.out.println("User input: " + userInput);

        userInput = in.readLine();

        System.out.println("Second: " + userInput);*/

//        List<String> words = new ArrayList<>();
//        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
//        words = resourceInputFileReader.readFile("dex.txt");

//        for (String word : words) {
//            System.out.println(word);
//        }
//
//        Set<String> wordsNoDuplicate = utils.Utils.removeDuplicates(words);
//        for (String word: wordsNoDuplicate){
//            System.out.println(word);
//        }

    }
}
