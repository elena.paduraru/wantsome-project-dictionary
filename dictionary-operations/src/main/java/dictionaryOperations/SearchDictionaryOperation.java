package dictionaryOperations;

import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Set;

//find all words which contains a specific substring
public class SearchDictionaryOperation implements DictionaryOperation{
    private Set<String> wordsSet;
    private BufferedReader in;

    public SearchDictionaryOperation(Set<String> wordsSet, BufferedReader in){
        this.in = in;
        this.wordsSet = wordsSet;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Searching....");

        String userInput = in.readLine();
        System.out.println("From user: " + userInput);

        Set<String> result = Utils.findWordsThatContainsASpecificString(wordsSet, userInput);

        for (String line : result){
            System.out.println(line);
        }

        System.out.println("Finished!!!");
    }
}
