package dictionaryOperations;

import utils.Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class AnagramDictionaryOperation implements DictionaryOperation {
    Set<String> wordsSet;
    BufferedReader in;

    public AnagramDictionaryOperation(Set<String> wordsSet, BufferedReader in) {
        this.wordsSet = wordsSet;
        this.in = in;
    }

    @Override
    public void run() throws IOException {
        System.out.println("Anagrams....");
        String userInput = in.readLine();

        Map<String, List<String>> anagrams = Utils.getAnagrams(wordsSet);
        if (anagrams.size() == 0) {
            System.out.println("No anagrams");
            return;
        }

        List<String> anagramsList = anagrams.get(Utils.sortLetters(userInput));
        System.out.println("print anagrams for " + userInput + ": ");

        for (String word : anagramsList) {
            System.out.println(word);
        }

        System.out.println("Done!");
    }
}
