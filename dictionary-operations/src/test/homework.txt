Add anagrams finding to your workshop project

As a dictionary user
I want to find all the anagrams of a word in the dictionary
So that I can easily solve word puzzles :)



Task: Add one more menu option to your workshop project to find all anagrams of a word and show them by groups.



Pseudocode
Sort the letters
Create a method that sorts all the letters in a word alphabetically.

Unit test this method.

Find the anagrams
Create a Map<String, List<String>>: this will have:

key: the word with the sorted letters
value: list of all the words that are made of the same letters
Go through all the words, and for each one:

sort the letters in the word
If the map has a key with this sorted word, add the original word to the value list
If the map doesn't have this key, create a new list, add the word to it, and add it to the map
Unit test this algorithm

Plug it in
Plugin the new code to the main function

For a given user word, display all it's anagrams.