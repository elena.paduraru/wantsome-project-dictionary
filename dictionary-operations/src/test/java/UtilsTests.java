import fileReader.ResourceInputFileReader;
import org.junit.Test;
import utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.*;

public class UtilsTests {

    @Test
    public void testPalindromeFinder(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("unu");
        Set<String> palindromes = Utils.findAllPalindromes(inputSet); // -> palindromes has "unu"

        assertTrue(palindromes.contains("unu"));
        assertTrue(palindromes.size() == 1);
        assertEquals(1, palindromes.size());
    }

    @Test
    public void testPalindromesFromFile() throws IOException {
        List<String> words = new ArrayList<>();
        ResourceInputFileReader resourceInputFileReader = new ResourceInputFileReader();
        words = resourceInputFileReader.readFile("test.txt");
        Set<String> wordsNoDuplicate = Utils.removeDuplicates(words);

        Set<String> palindromes = Utils.findAllPalindromes(wordsNoDuplicate);

        assertTrue(palindromes.contains("aba"));
        assertFalse(palindromes.contains("trei"));

        assertTrue(palindromes.size() == 3);
    }

    @Test
    public void testPalindromesNoResult(){
        Set<String> inputSet = new HashSet<>();
        inputSet.add("telfon");

        Set<String> palindromes = Utils.findAllPalindromes(inputSet);
        assertTrue(palindromes.size() == 0);
    }
}
